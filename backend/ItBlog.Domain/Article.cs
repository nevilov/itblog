﻿using ItBlog.Domain.Shared;
using System;

namespace ItBlog.Domain
{
    /// <summary>
    /// Сущность статья
    /// </summary>
    public class Article : BaseEntity<Guid>
    {
        /// <summary>
        /// Заголовок статьи
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Описание статьи
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Идентификатор автора
        /// </summary>
        public Guid AuthorId { get; set; }

        /// <summary>
        /// Автор
        /// </summary>
        public Profile Author { get; set; }


        //Category(Label)

        //Raiting
    }
}
