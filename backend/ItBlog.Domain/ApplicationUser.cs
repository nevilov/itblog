﻿using Microsoft.AspNetCore.Identity;
using System;

namespace ItBlog.Domain
{
    public class ApplicationUser : IdentityUser<Guid>
    {
    }
}
