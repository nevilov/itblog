﻿namespace ItBlog.Domain.Shared.Exceptions
{
    public class HaveNoRightsException : DomainException
    {
        public HaveNoRightsException(string message) : base(message)
        {
        }
    }
}
