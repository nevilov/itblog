﻿namespace ItBlog.Domain.Shared.Exceptions
{
    public class DuplicateException : DomainException
    {
        public DuplicateException(string message) : base(message)
        {
        }
    }
}
