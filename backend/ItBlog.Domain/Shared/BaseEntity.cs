﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItBlog.Domain.Shared
{
    public class BaseEntity<TId>
    {
        /// <summary>
        /// Уникальный идентификатор сущности
        /// </summary>
        public TId Id { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedDate { get; set; }
        
        /// <summary>
        /// Дата обновления
        /// </summary>
        public DateTime UpdatedDate { get; set; }

        /// <summary>
        /// Дата удаления
        /// </summary>
        public DateTime RemovedDate { get; set; }
    }
}
