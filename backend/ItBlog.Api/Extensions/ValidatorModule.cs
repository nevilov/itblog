﻿using FluentValidation;
using FluentValidation.AspNetCore;
using ItBlog.Api.Validators.Article;
using ItBlog.Api.Validators.User;
using ItBlog.Contracts.Dtos.Article;
using ItBlog.Contracts.Dtos.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace ItBlog.Api.Extensions
{
    public static class ValidatorModule
    {
        public static IServiceCollection AddValidatorModule(this IServiceCollection services)
        {
            services.AddFluentValidation();

            services.AddTransient<IValidator<CreateArticle.Request>, CreateArticleValidator>();
            services.AddTransient<IValidator<Login.Request>, LoginValidator>();
            services.AddTransient<IValidator<Register.Request>, RegisterValidator>();

            return services;
        }
    }
}
