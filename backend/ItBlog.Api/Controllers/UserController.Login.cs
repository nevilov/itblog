﻿using ItBlog.Contracts.Dtos.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace ItBlog.Api.Controllers
{
    public partial class UserController
    {
        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(Login.Request request, CancellationToken cancellationToken)
        {
            var token = await _identityService.Login(request, cancellationToken);

            return Ok(token);
        } 
    }
}
