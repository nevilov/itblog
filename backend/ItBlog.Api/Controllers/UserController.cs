﻿using ItBlog.Application.Identity.Interfaces;
using ItBlog.Application.Services.Profile.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ItBlog.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public partial class UserController : ControllerBase
    {
        private readonly IProfileService _profileService;
        private readonly IIdentityService _identityService;

        public UserController(IProfileService profileService,
            IIdentityService identityService)
        {
            _profileService = profileService;
            _identityService = identityService;
        }
    }
}
