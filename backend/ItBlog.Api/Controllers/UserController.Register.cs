﻿using ItBlog.Contracts.Dtos.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace ItBlog.Api.Controllers
{
    public partial class UserController
    {
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register(Register.Request request, CancellationToken cancellationToken)
        {
            await _profileService.Register(request, cancellationToken);
            return StatusCode(201);
        }
    }
}
