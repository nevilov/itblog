﻿using System.Threading;
using System.Threading.Tasks;
using ItBlog.Contracts.Dtos.Article;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ItBlog.Api.Controllers
{
    public partial class ArticleController
    {
        [HttpPut]
        [Route("update")]
        [Authorize]
        public async Task<IActionResult> Update(UpdateArticle.Request request, CancellationToken cancellationToken)
        {
            await _articleService.UpdateArticle(request, cancellationToken);
            return Ok();
        }
    }
}
