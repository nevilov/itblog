﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace ItBlog.Api.Controllers
{
    public partial class ArticleController
    {
        [HttpGet("get/{id}")]
        public async Task<IActionResult> GetById([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            var result = await _articleService.GetArticleById(id, cancellationToken);

            return Ok(result);
        }
    }
}
