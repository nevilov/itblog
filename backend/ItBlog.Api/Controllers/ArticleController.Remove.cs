﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ItBlog.Api.Controllers
{
    public partial class ArticleController
    {
        [HttpDelete]
        [Route("remove/{id}")]
        public async Task<IActionResult> Remove([FromRoute]Guid id, CancellationToken cancellationToken)
        {
            await _articleService.RemoveArticle(id, cancellationToken);
            return Ok();
        }
    }
}
