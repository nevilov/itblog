﻿using ItBlog.Application.Services.Article.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ItBlog.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public partial class ArticleController : ControllerBase
    {
        private readonly IArticleService _articleService;

        public ArticleController(IArticleService articleService)
        {
            _articleService = articleService;
        }


    }
}
