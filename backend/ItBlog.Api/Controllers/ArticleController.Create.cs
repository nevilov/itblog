﻿using ItBlog.Contracts.Dtos.Article;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace ItBlog.Api.Controllers
{
    public partial class ArticleController
    {
        [HttpPost]
        [Route("create")]
        [Authorize]
        public async Task<IActionResult> Create(CreateArticle.Request request, CancellationToken cancellationToken)
        {
            await _articleService.CreateArticle(request, cancellationToken);
            return StatusCode(201);
        }
    }
}
