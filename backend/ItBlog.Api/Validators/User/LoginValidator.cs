﻿using FluentValidation;
using ItBlog.Contracts.Dtos.Identity;

namespace ItBlog.Api.Validators.User
{
    public class LoginValidator : AbstractValidator<Login.Request>
    {
        public LoginValidator()
        {
            RuleFor(x => x.Login)
                .NotEmpty().WithMessage("Заполните это поле!");
        }
    }
}
