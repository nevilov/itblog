﻿using FluentValidation;
using ItBlog.Contracts.Common;
using ItBlog.Contracts.Dtos.Identity;
using ItBlog.Domain;
using Microsoft.AspNetCore.Identity;

namespace ItBlog.Api.Validators.User
{
    public class RegisterValidator : AbstractValidator<Register.Request>
    {
        public RegisterValidator(UserManager<ApplicationUser> userManager)
        {
            RuleFor(x => x.Email)
                .EmailAddress().WithMessage("Поле должно содержать email")
                .MustAsync(async (value, token) =>
                {
                    var existingUser = await userManager.FindByEmailAsync(value);
                    return existingUser == null;
                }).WithMessage("Данный email занят");

            RuleFor(x => x.UserName)
                .MinimumLength(5).WithMessage("Юзернейм не может быть меньше 5 символов")
                .MaximumLength(15).WithMessage("Юзернейм не может быть больше 15 символов")
                .MustAsync(async (value, token) =>
                {
                    var existingUser = await userManager.FindByNameAsync(value);
                    return existingUser == null;
                }).WithMessage("Данный UserName занят");

            RuleFor(x => x.Password)
                .NotEmpty()
                .MinimumLength(6)
                .Matches("[^a-zA-Z0-9]").WithMessage("Пароль должен содержать строчные и заглавные буквы латинского алфавита, а так же цифры");
        }
    }
}
