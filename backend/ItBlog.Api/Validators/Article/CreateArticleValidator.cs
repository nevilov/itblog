﻿using FluentValidation;
using ItBlog.Contracts.Common;
using ItBlog.Contracts.Dtos.Article;

namespace ItBlog.Api.Validators.Article
{
    public class CreateArticleValidator : AbstractValidator<CreateArticle.Request>
    {
        public CreateArticleValidator()
        {
            RuleFor(x => x.Title)
                .MinimumLength(ValidatorConstants.MIN_LENGTH).WithMessage($"Минимальное количество символов для загловка статьи ({ValidatorConstants.MIN_LENGTH})")
                .MaximumLength(ValidatorConstants.MAX_LENGTH_SHORT).WithMessage($"Максимальное количество символов для заголовка статьи({ValidatorConstants.MAX_LENGTH_SHORT})");

            RuleFor(x => x.Description)
                .MinimumLength(ValidatorConstants.MIN_LENGTH).WithMessage($"Минимальное количество символов для описания ({ValidatorConstants.MIN_LENGTH})")
                .MaximumLength(ValidatorConstants.MAX_LENGTH_LONG).WithMessage($"Максимальное количество символов для описания ({ValidatorConstants.MAX_LENGTH_LONG})");
        }
    }
}
