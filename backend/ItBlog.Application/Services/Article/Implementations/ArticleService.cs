﻿using ItBlog.Application.Identity.Interfaces;
using ItBlog.Application.Repositories;
using ItBlog.Application.Services.Article.Interfaces;
using ItBlog.Contracts.Dtos.Article;
using ItBlog.Domain.Shared.Exceptions;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ItBlog.Application.Services.Article.Implementations
{
    public class ArticleService : IArticleService
    {
        private readonly IRepository<Domain.Article, Guid> _articleRepository;
        private readonly IRepository<Domain.Profile, Guid> _profileRepository;
        private readonly IIdentityService _identityService;

        public ArticleService(IRepository<Domain.Article, Guid> repository,
            IIdentityService identityService,
            IRepository<Domain.Profile, Guid> profileRepository)
        {
            _articleRepository = repository;
            _identityService = identityService;
            _profileRepository = profileRepository;
        }

        public async Task CreateArticle(CreateArticle.Request request, CancellationToken cancellationToken)
        {
            var userId = Guid.Parse(await _identityService.GetCurrentUserId(cancellationToken));
            var user = await _profileRepository.FindById(userId, cancellationToken);

            var article = new Domain.Article
            {
                Id = Guid.NewGuid(),
                Description = request.Description,
                Title = request.Title,
                CreatedDate = DateTime.UtcNow,
                AuthorId = userId,
                Author = user
            };

            await _articleRepository.Save(article, cancellationToken);
        }

        public async Task<GetArticleById.Response> GetArticleById(Guid id, CancellationToken cancellationToken)
        {
            var article = await _articleRepository.FindById(id, cancellationToken);

            return new GetArticleById.Response
            {
                Title = article.Title,
                Description = article.Description
            };
        }

        public async Task RemoveArticle(Guid id, CancellationToken cancellationToken)
        {
            var article = await _articleRepository.FindById(id, cancellationToken);
            if(article == null)
            {
                throw new NotFoundException($"Статья с id {id} не была найдена");
            }
            article.RemovedDate = DateTime.UtcNow;

            await _articleRepository.Save(article, cancellationToken);
        }

        public async Task UpdateArticle(UpdateArticle.Request request, CancellationToken cancellationToken)
        {
            var article = await _articleRepository.FindById(request.Id, cancellationToken);

            if (article == null)
            {
                throw new NotFoundException($"Статья с id {request.Id} не была найдена");
            }

            article.Title = request.Title;
            article.Description = request.Description;
            article.UpdatedDate = DateTime.UtcNow;
        }
    }
}
