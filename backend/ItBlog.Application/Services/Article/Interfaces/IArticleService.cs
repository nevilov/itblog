﻿using ItBlog.Contracts.Dtos.Article;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ItBlog.Application.Services.Article.Interfaces
{
    public interface IArticleService
    {
        public Task CreateArticle(CreateArticle.Request request, CancellationToken cancellationToken);

        public Task<GetArticleById.Response> GetArticleById(Guid id, CancellationToken cancellationToken);

        public Task RemoveArticle(Guid id, CancellationToken cancellationToken);

        public Task UpdateArticle(UpdateArticle.Request request, CancellationToken cancellationToken);
    }
}
