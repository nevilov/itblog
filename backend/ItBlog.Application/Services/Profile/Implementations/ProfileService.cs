﻿using ItBlog.Application.Identity.Interfaces;
using ItBlog.Application.Repositories;
using ItBlog.Application.Services.Profile.Interfaces;
using ItBlog.Contracts.Dtos.Identity;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ItBlog.Application.Services.Profile.Implementations
{
    public class ProfileService : IProfileService
    {
        private readonly IRepository<Domain.Profile, Guid> _repository;
        private readonly IIdentityService _identityService;

        public ProfileService(IRepository<Domain.Profile, Guid> repository,
            IIdentityService identityService)
        {
            _repository = repository;
            _identityService = identityService;
        }

        public async Task Register(Register.Request request, CancellationToken cancellationToken)
        {
            var idCreatedUser = await _identityService.CreateUser(request, cancellationToken);

            var profile = new Domain.Profile
            {
                Id = idCreatedUser,
                Name = request.Name,
                CreatedDate = DateTime.UtcNow
            };

            await _repository.Save(profile, cancellationToken);
        }
    }
}
