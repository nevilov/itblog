﻿using ItBlog.Contracts.Dtos.Identity;
using System.Threading;
using System.Threading.Tasks;

namespace ItBlog.Application.Services.Profile.Interfaces
{
    public interface IProfileService
    {
        public Task Register(Register.Request request, CancellationToken cancellationToken);
    }
}
