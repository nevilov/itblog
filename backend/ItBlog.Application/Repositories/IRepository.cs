﻿using ItBlog.Domain.Shared;
using System.Threading;
using System.Threading.Tasks;

namespace ItBlog.Application.Repositories
{
    public interface IRepository<TEntity, TId>
        where TEntity : BaseEntity<TId>
    {
        public Task<TEntity> FindById(TId id, CancellationToken cancellationToken);

        public Task Save(TEntity entity, CancellationToken cancellationToken);
    }
}
