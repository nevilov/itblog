﻿using ItBlog.Contracts.Dtos.Identity;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ItBlog.Application.Identity.Interfaces
{
    public interface IIdentityService
    {
        public Task<string> GetCurrentUserId(CancellationToken cancellationToken);

        public Task<Login.Response> Login(Login.Request request, CancellationToken cancellationToken);

        public Task<Guid> CreateUser(Register.Request request, CancellationToken cancellationToken);
    }
}
