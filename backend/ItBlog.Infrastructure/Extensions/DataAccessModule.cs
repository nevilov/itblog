﻿using ItBlog.Application.Repositories;
using ItBlog.Infrastructure.DataAccess;
using ItBlog.Infrastructure.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ItBlog.Infrastructure.Extensions
{
    public static class DataAccessModule
    {
        public static IServiceCollection AddDataAccessModule(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ItBlogDbContext>(p =>
            {
                p.UseNpgsql(configuration.GetConnectionString("PostgresDb"));
            });

            services
                .AddScoped<IRepository<Domain.Article, Guid>, Repository<Domain.Article, Guid>>()
                .AddScoped<IRepository<Domain.Profile, Guid>, Repository<Domain.Profile, Guid>>();

            return services;
        }
    }
}
