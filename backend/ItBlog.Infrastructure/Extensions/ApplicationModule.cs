﻿using ItBlog.Application.Identity.Interfaces;
using ItBlog.Application.Services.Article.Implementations;
using ItBlog.Application.Services.Article.Interfaces;
using ItBlog.Application.Services.Profile.Implementations;
using ItBlog.Application.Services.Profile.Interfaces;
using ItBlog.Infrastructure.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace ItBlog.Infrastructure.Extensions
{
    public static class ApplicationModule
    {
        public static IServiceCollection AddApplicationModule(this IServiceCollection services)
        {
            services
                .AddScoped<IArticleService, ArticleService>()
                .AddScoped<IProfileService, ProfileService>();

            services
                .AddScoped<IIdentityService, IdentityService>();
            
            return services;
        }
    }
}
