﻿using ItBlog.Application.Identity.Interfaces;
using ItBlog.Contracts.Dtos.Identity;
using ItBlog.Domain;
using ItBlog.Domain.Shared.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ItBlog.Infrastructure.Identity
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;

        public IdentityService(UserManager<ApplicationUser> userManager,
            IHttpContextAccessor httpContextAccessor,
            IConfiguration configuration)
        {
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
        }

        public Task<string> GetCurrentUserId(CancellationToken cancellationToken)
        {
            var claimsPrincipal = _httpContextAccessor.HttpContext?.User;
            return Task.FromResult(_userManager.GetUserId(claimsPrincipal));
        }

        public async Task<Login.Response> Login(Login.Request request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(request.Login);
            if(user == null)
            {
                throw new NotFoundException("Неправильный логин или пароль");
            }

            var isPasswordCorrect = await _userManager.CheckPasswordAsync(user, request.Password);
            if (!isPasswordCorrect)
            {
                throw new HaveNoRightsException("Неправильный логин или пароль");
            }

            var claims = new List<Claim>
             {
                 new Claim(ClaimTypes.Email, user.Email),
                 new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                 new(ClaimTypes.Name, user.UserName),
                 new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
             };

            var userRoles = await _userManager.GetRolesAsync(user);
            claims.AddRange(userRoles.Select(role => new Claim(ClaimTypes.Role, role)));

            var token = new JwtSecurityToken
            (
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(15),
                issuer: _configuration["Jwt:ValidIssuer"],
                audience: _configuration["Jwt:ValidAudience"],
                notBefore: DateTime.UtcNow,
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:SecretKey"])),
                    SecurityAlgorithms.HmacSha256
                )
            );

            return new Login.Response
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token)
            };

        }

        public async Task<Guid> CreateUser(Register.Request request, CancellationToken cancellationToken)
        {
            var user = new ApplicationUser
            {
                Id = new Guid(),
                Email = request.Email,
                UserName = request.UserName,
            };
            await _userManager.CreateAsync(user);
            await _userManager.AddPasswordAsync(user, request.Password);

            return user.Id;
        }
    }
}
