﻿using ItBlog.Domain;
using ItBlog.Infrastructure.DataAccess.Configurations;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace ItBlog.Infrastructure.DataAccess
{
    public class ItBlogDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        public DbSet<Article> Articles { get; set; }
        public DbSet<Profile> Profiles { get; set; }

        public ItBlogDbContext(DbContextOptions<ItBlogDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating (ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new IdentityConfiguration());
            IdentityConfiguration.SeedRolesData(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

    }

}
