﻿using ItBlog.Application.Repositories;
using ItBlog.Domain.Shared;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace ItBlog.Infrastructure.DataAccess.Repositories
{
    public class Repository<TEntity, TId> : IRepository<TEntity, TId>
        where TEntity : BaseEntity<TId>
    {
        protected readonly ItBlogDbContext _dbContext;

        public Repository(ItBlogDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<TEntity> FindById(TId id, CancellationToken cancellationToken)
        {
            return await _dbContext.Set<TEntity>().FindAsync(new object[]{id}, cancellationToken);
        }


        public async Task Save(TEntity entity, CancellationToken cancellationToken)
        {
            var entry = _dbContext.Entry(entity);

            if (entry.State == EntityState.Detached)
            {
                await _dbContext.Set<TEntity>().AddAsync(entity, cancellationToken);
            }

            await _dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
