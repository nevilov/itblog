﻿using ItBlog.Contracts.Common;
using ItBlog.Domain;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace ItBlog.Infrastructure.DataAccess.Configurations
{
    public class IdentityConfiguration : IEntityTypeConfiguration<ApplicationUser>
    {
        static Guid ADMIN_ID = Guid.Parse("38EEA783-902F-41F3-ADED-8C27DEE079E9");
        static Guid ADMIN_ROLE_ID = Guid.Parse("2CC65A96-496D-4E44-BDE1-90DE9BFCF2D4");
        static Guid USER_ROLE_ID = Guid.Parse("214AB4F1-B36B-4166-8F04-E59B7A998935");

        public void Configure(EntityTypeBuilder<ApplicationUser> builder)
        {
            var admin = new ApplicationUser
            {
                Id = ADMIN_ID,
                UserName = "admin",
                Email = "ADMIN@ADMIN.RU",
                EmailConfirmed = true,
            };
            var passwordHashForAdmin = new PasswordHasher<ApplicationUser>().HashPassword(admin, "admin");
            admin.PasswordHash = passwordHashForAdmin;

            builder.HasData(admin);
        }

        public static void SeedRolesData(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ApplicationRole>(x =>
            {
                x.HasData(new ApplicationRole[]
                {
                    new ApplicationRole
                    {
                        Id = ADMIN_ROLE_ID,
                        Name = RoleContstants.Admin,
                        NormalizedName = RoleContstants.Admin.ToUpper(),
                    },

                    new ApplicationRole
                    {
                        Id = USER_ROLE_ID,
                        Name = RoleContstants.User,
                        NormalizedName = RoleContstants.User.ToUpper()
                    }
                });
            });

            modelBuilder.Entity<IdentityUserRole<Guid>>(x =>
            {
                x.HasData(new IdentityUserRole<Guid>
                {
                    RoleId = ADMIN_ROLE_ID,
                    UserId = ADMIN_ID
                });
            });

            modelBuilder.Entity<Profile>(x =>
            {
                x.HasData(new Profile
                {
                    Id = ADMIN_ID,
                    Name = "Admin",
                    CreatedDate = DateTime.UtcNow
                });
            });
        }
    }
}
