﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ItBlog.Infrastructure.Migrations
{
    public partial class AddProfiles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Profiles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Status = table.Column<string>(type: "text", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    RemovedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Profiles", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("214ab4f1-b36b-4166-8f04-e59b7a998935"),
                column: "ConcurrencyStamp",
                value: "63544bcf-bcb2-446f-b660-b2f170579ca6");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("2cc65a96-496d-4e44-bde1-90de9bfcf2d4"),
                column: "ConcurrencyStamp",
                value: "ffd323b0-c0d6-4bae-9f40-9c26cf22951b");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "e5a79c84-c2ab-4751-8e0a-f4980e793f1b", "AQAAAAEAACcQAAAAEPtLrXegRgRf4NmXqjxPnXJfjccJNxapeYepoXryur2gQ2WCmrG0CcyJbZ911cH1Xw==" });

            migrationBuilder.InsertData(
                table: "Profiles",
                columns: new[] { "Id", "CreatedDate", "Name", "RemovedDate", "Status", "UpdatedDate" },
                values: new object[] { new Guid("38eea783-902f-41f3-aded-8c27dee079e9"), new DateTime(2021, 6, 12, 16, 41, 7, 118, DateTimeKind.Utc).AddTicks(5293), "Admin", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Profiles");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("214ab4f1-b36b-4166-8f04-e59b7a998935"),
                column: "ConcurrencyStamp",
                value: "0483263f-0f5f-47e7-a6e8-21b7643f1289");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("2cc65a96-496d-4e44-bde1-90de9bfcf2d4"),
                column: "ConcurrencyStamp",
                value: "e9e79d99-8739-46b6-9845-24ffbab58705");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "f5ec4b9c-ce51-451e-a852-d7d72f89148b", "AQAAAAEAACcQAAAAECUAr4u4prFVynzvzkeDd5QDiP6faWwOB9F/hLgWlRRzqlQd1ALmRt85+W1RPoLpPQ==" });
        }
    }
}
