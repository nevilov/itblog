﻿namespace ItBlog.Contracts.Common
{
    public static class ValidatorConstants
    {
        public static int MIN_LENGTH = 10;

        public static int MAX_LENGTH_SHORT = 100;
        public static int MAX_LENGTH_LONG = 1000;
    }
}
