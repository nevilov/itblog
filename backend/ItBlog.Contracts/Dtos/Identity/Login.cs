﻿namespace ItBlog.Contracts.Dtos.Identity
{
    public static class Login
    {
        public class Request
        {
            public string Login { get; set; }

            public string Password { get; set; }
        }

        public class Response
        {
            public string Token { get; set; }
        }
    }
}
