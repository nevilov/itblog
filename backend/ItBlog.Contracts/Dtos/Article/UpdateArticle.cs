﻿using System;
namespace ItBlog.Contracts.Dtos.Article
{
    public static class UpdateArticle
    {
        public class Request
        {
            public Guid Id { get; set; }

            public string Title { get; set; }

            public string Description { get; set; }
        }
    }
}
