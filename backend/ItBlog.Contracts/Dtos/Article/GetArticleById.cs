﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ItBlog.Contracts.Dtos.Article
{
    public static class GetArticleById
    {
        public class Request
        {
            public Guid Id { get; set; }
        }

        public class Response
        {
            public string Title { get; set; }
            
            public string Description { get; set; }
        }
    }
}
