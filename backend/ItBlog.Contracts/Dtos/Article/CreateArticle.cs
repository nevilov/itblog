﻿namespace ItBlog.Contracts.Dtos.Article
{
    public static class CreateArticle
    {
        public class Request
        {
            public string Title { get; set; }

            public string Description { get; set; }
        }

        public class Response
        {

        }
    }
}
